## OLLXSLT ##

The ollxslt application uses xsl scripts to transform book xml
into a json format.  It will also download the xml and http files
for an oll title.

usage: `node ollxslt -s <slug> -f <json|html|xml> -o </dir/for/results>`

The slug is the only required parameter. The format defaults to json
and the output directory is the current working directory.

The script will create the following files
1. <orig file name>.xdm.xml: xdm formatted document that allows the xsl3 xml-to-json to work
2. <orig file name>.json: json output of xdm


### prerequisites ###

- java runtime >8.0. Also set the JAVA_HOME environment variable to the jre root directory
- npm most recent version
- nodejs must be version 12.00 or greater

### How do I get set up? ###

```
$ git clone https://libertyfund@bitbucket.org/libertyfund/ollxslt.git
$ cd ollxslt
$ npm install 
```

### xml/xsl sources ###



