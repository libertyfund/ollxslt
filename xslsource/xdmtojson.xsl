<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    exclude-result-prefixes="xs math saxon"
    xmlns="http://www.w3.org/2005/xpath-functions"
    version="3.0" xmlns:saxon="http://saxon.sf.net/">
    <!-- The input JSON file -->
    <xsl:output method="text" indent="yes" media-type="text/json" omit-xml-declaration="yes"/>
    <saxon:output indent="yes" saxon:indent-spaces="4"/>

    <xsl:strip-space elements="*"/>
    
    <!-- The initial template that process the JSON -->
    <xsl:template match="/"> <!-- name="xsl:initial-template"> -->
        <xsl:apply-templates select="xml-to-json(.,map { 'indent': true()})"/>
    </xsl:template>
</xsl:stylesheet>
