<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xs math xd"
    xmlns="http://www.w3.org/2005/xpath-functions"
    version="3.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Sep 7, 2021</xd:p>
            <xd:p><xd:b>Author:</xd:b> jfcote</xd:p>
            <xd:p/>
        </xd:desc>
    </xd:doc>

    <xsl:output media-type="text/xml" indent="yes"/>

    <xd:doc>
        <xd:desc>Remove padding spaces for all elements</xd:desc>
    </xd:doc>
    <xsl:strip-space elements="*"/>

    <xsl:param name="paraprefix"/>

    <xd:docdoc>
        <xd:desc>charpos tracks a character count over all text excluding notes</xd:desc>
    </xd:docdoc>
    <xsl:accumulator name="charpos" as="xs:integer" initial-value="0">
        <xsl:accumulator-rule phase="end" match="//document//self::text()[not(ancestor::note[1]) and not(ancestor::figure[1])]" select="$value + string-length(.)"/>
    </xsl:accumulator>
    
    <xd:doc>
        <xd:desc>paracnt for all non-id'd para</xd:desc>
    </xd:doc>
    <xsl:accumulator name="paracnt" as="xs:integer" initial-value="0">
        <!-- <xsl:accumulator-rule phase="end" match="div[@type='para']" select="$value + 1"/> -->
        <xsl:accumulator-rule phase="start" select="$value + 1" match="byline | item[not(ancestor::note[1]|ancestor::figure[1]|@id)] |
            verso | p[not(ancestor::note[1]|ancestor::figure[1]|@id)] | l[not(ancestor::note[1]|ancestor::figure[1]|@id)] | headword[not(ancestor::note[1]|ancestor::figure[1]|@id)] |
            pages[not(ancestor::note[1]|ancestor::figure[1]|@id)] | seealso[not(ancestor::note[1]|ancestor::figure[1]|@id)] | 
            descrip[not(ancestor::note[1]|ancestor::figure[1]|@id)]  | td[not(ancestor::note[1]|ancestor::figure[1]|@id)] | th[not(ancestor::note[1]|ancestor::figure[1]|@id)] |
            title[not(ancestor::note[1]|ancestor::figure[1]|@id)] | rdg[not(ancestor::note[1]|ancestor::figure[1]|@id)] ">
        </xsl:accumulator-rule>
    </xsl:accumulator>

    <xd:doc>
        <xd:desc>marginnote adds unique ids for <notes type="margin"/> without an assigned id</xd:desc>
    </xd:doc>
   <xsl:accumulator name="marginnote" as="xs:integer" initial-value="0">
       <xsl:accumulator-rule phase="start" select="$value + 1" match="note[@type='margin' and not(@id)]"/>
   </xsl:accumulator>
    

    <xsl:mode use-accumulators="charpos paracnt marginnote"/>
    
    <xd:doc>
        <xd:desc>start matching and create an array document nodes. Next
            create maps of note nodes and ids</xd:desc>
    </xd:doc>
    <xsl:template match="/">
        <xsl:element name="map">
            <xsl:element name="array">
                <xsl:attribute name="key">document</xsl:attribute>
                <xsl:apply-templates select="/publication/document/*"/>
            </xsl:element>
            <xsl:element name="map">
                <xsl:attribute name="key">notes</xsl:attribute>
                <xsl:variable name="notelist" select="//note"/>
                <xsl:variable name="notetypes" select="distinct-values(//note/@type)"/>
                <xsl:for-each select="$notetypes">
                    <xsl:variable name="notetype" select="string(.)"/>
                    <xsl:element name="map">
                        <xsl:attribute name="key"><xsl:value-of select="$notetype"/></xsl:attribute>
                        <xsl:apply-templates select="$notelist[@type=$notetype]" mode="notes"/>
                    </xsl:element>
                </xsl:for-each>
            </xsl:element>
        </xsl:element>
    </xsl:template>


    <xd:doc>
        <xd:desc>ignore status=code</xd:desc>
    </xd:doc>
    <xsl:template match="@status[.='coded']"/>
    
    <xd:doc>
        <xd:desc>ignore status=code</xd:desc>
    </xd:doc>
    <xsl:template match="@numbered[.='no']"/>
    
    <xd:doc>
        <xd:desc>ignore HTML attribute</xd:desc>
    </xd:doc>    <xsl:template match="@HTML"/>
    
    <xd:doc>
        <xd:desc>ignore lang=eng</xd:desc>
    </xd:doc>
    <xsl:template match="@lang[.='eng']"/>
    
    <xd:doc>
        <xd:desc>default attributes create a string element with key = @name and
        element value = attribute value</xd:desc>
    </xd:doc>
    <xsl:template match="@*">
        <xsl:element name="string"> 
            <xsl:attribute name="key"><xsl:value-of select="local-name()"/></xsl:attribute>
            <xsl:value-of select="string(.)"/>
        </xsl:element>
    </xsl:template>

    <xd:doc>
        <xd:desc>stringelement creates string element</xd:desc>
        <xd:param name="keynm">xs:string key for map</xd:param>
        <xd:param name="value">xs:string value as content</xd:param>
    </xd:doc>
    <xsl:template name="stringelement">
        <xsl:param name="keynm" as="xs:string"/>
        <xsl:param name="value" as="xs:string"/>
        <xsl:element name="string">
            <xsl:attribute name="key" select="$keynm"/>
            <xsl:value-of select="$value"/>        
        </xsl:element>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>startmap output default tags for a block</xd:desc>
        <xd:param name="objtype">xs:string to set type element in map</xd:param>
        <xd:param name="tag">xs:string tag of calling node</xd:param>
    </xd:doc>
    <xsl:template name="startmap">
        <xsl:param name="objtype" as="xs:string"/>
        <xsl:param name="tag" as="xs:string"/>
        <xsl:call-template name="stringelement">
            <xsl:with-param name="keynm">object</xsl:with-param>
            <xsl:with-param name="value"><xsl:value-of select="$objtype"/></xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="stringelement">
            <xsl:with-param name="keynm">tag</xsl:with-param>
            <xsl:with-param name="value" select="$tag"/>
        </xsl:call-template>
        <xsl:if test="preceding::pb[1]">
        <xsl:call-template name="stringelement">
            <xsl:with-param name="keynm">page</xsl:with-param>
            <xsl:with-param name="value" select="preceding::pb[1]/@n"/>
        </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>figure handles a figure and resources</xd:desc>
    </xd:doc>
    <xsl:template match="figure">
        <xsl:element name="map">
            <xsl:call-template name="startmap">
                <xsl:with-param name="objtype">block</xsl:with-param>
                <xsl:with-param name="tag">figure</xsl:with-param>
            </xsl:call-template>
            <xsl:apply-templates select="@*"/>
            <xsl:element name="array">
                <xsl:attribute name="key">children</xsl:attribute>
                <xsl:apply-templates select="*"/>
            </xsl:element>
        </xsl:element>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>handle resources for figure</xd:desc>
    </xd:doc>
    <!-- <xsl:template match="*[ancestor::figure[1]]">
        <xsl:element name="map">
            <xsl:call-template name="startmap">
                <xsl:with-param name="objtype">resource</xsl:with-param>
                <xsl:with-param name="tag"><xsl:value-of select="local-name()"/></xsl:with-param>
            </xsl:call-template>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:element>
    </xsl:template> -->

    <xd:doc>
        <xd:desc>skip over containers for text elements</xd:desc>
    </xd:doc>
    <xsl:template match="doctitle  | docimprint | lg | div[@type='para']">
        <xsl:apply-templates select="*"/>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>handle head and subhead tags.  Array of text runs for children</xd:desc>
    </xd:doc>
    <xsl:template match="head|subhead">
        <xsl:element name="map">
            <xsl:call-template name="startmap">
                <xsl:with-param name="objtype">para</xsl:with-param>
                <xsl:with-param name="tag" select="local-name()"/>
            </xsl:call-template>
            <xsl:apply-templates select="@*"/>
            <xsl:element name="array">
                <xsl:attribute name="key">runs</xsl:attribute>
                <xsl:apply-templates select="node()" mode="para"/>
            </xsl:element>
        </xsl:element>
    </xsl:template>

    <xd:doc>
        <xd:desc>containers of text() node.  Flatten children into runs</xd:desc>
    </xd:doc>
    <xsl:template match="
            byline | docauthor  | 
            epigraph | titlepart | docdate | docedition | p | item | verso |  l |
            headword | pages | seealso | descrip | label | td | th ">
        <xsl:element name="map">
            <xsl:call-template name="startmap">
                <xsl:with-param name="objtype">para</xsl:with-param>
                <xsl:with-param name="tag" select="local-name()"/>
            </xsl:call-template>
            <xsl:if test="not(@id) and not(ancestor::note[1]) and not(ancestor::front[1]) and not(ancestor::figure[1])">
                <xsl:call-template name="stringelement">
                   <xsl:with-param name="keynm">id</xsl:with-param>
                    <xsl:with-param name="value" select="concat($paraprefix,'_',accumulator-before('paracnt'))"/>                
                </xsl:call-template>
            </xsl:if>
            <xsl:apply-templates select="@*"/>
            <xsl:element name="array">
                <xsl:attribute name="key">runs</xsl:attribute>
                <xsl:apply-templates select="node()" mode="para"/>
            </xsl:element>
        </xsl:element>
    </xsl:template>

    <xd:doc>
        <xd:desc>template matches all other blocks, generally divs</xd:desc>
    </xd:doc>
    <xsl:template match="*">
        <xsl:element name="map">
            <xsl:call-template name="startmap">
                <xsl:with-param name="objtype">block</xsl:with-param>
                <xsl:with-param name="tag" select="local-name()"/>
            </xsl:call-template>
            <xsl:apply-templates select="@*"/>
            <xsl:element name="array">
                <xsl:attribute name="key">children</xsl:attribute>            
                <xsl:apply-templates select="*"/>
            </xsl:element>
        </xsl:element>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>milestone tag added as a run with no text</xd:desc>
    </xd:doc>
    <xsl:template match="milestone" mode="para">
        <xsl:element name="map">
            <xsl:call-template name="startmap">
                <xsl:with-param name="objtype">run</xsl:with-param>
                <xsl:with-param name="tag" select="local-name()"/>
            </xsl:call-template>
            <xsl:apply-templates select="@*"/>
        </xsl:element>
    </xsl:template>

    <xd:doc>
        <xd:desc>text nodes are added as run with tags and attributes added from parent</xd:desc>
        <xd:param name="tag">tag of parent node.  If blank then no parent</xd:param>
        <xd:param name="attr">attributes from parent</xd:param>
    </xd:doc>
    <xsl:template match="self::text()" mode="para">
        <xsl:param name="tag"/>
        <xsl:param name="attr"/>
        <xsl:element name="map">
            <xsl:call-template name="startmap">
                <xsl:with-param name="objtype">run</xsl:with-param>
                <xsl:with-param name="tag" select="$tag"/>                
            </xsl:call-template>
            <xsl:if test="not(ancestor::note[1]|ancestor::figure[1]|@id)">
                <xsl:call-template name="stringelement">
                    <xsl:with-param name="keynm">pos</xsl:with-param>
                    <xsl:with-param name="value"><xsl:value-of select="accumulator-before('charpos')"/></xsl:with-param>                
                </xsl:call-template>
            </xsl:if>
            <xsl:if test="$attr">
                <xsl:apply-templates select="$attr"/>
                <!-- <xsl:for-each select="$attr">
                    <xsl:element name="string"> 
                        <xsl:attribute name="key"><xsl:value-of select="local-name(.)"/></xsl:attribute>
                        <xsl:copy><xsl:copy-of select="."/></xsl:copy>
                    </xsl:element>
                </xsl:for-each> -->
            </xsl:if>
            <xsl:call-template name="stringelement">
                <xsl:with-param name="keynm">text</xsl:with-param>
                <xsl:with-param name="value"><xsl:value-of select="."/></xsl:with-param>
            </xsl:call-template>            
        </xsl:element>
    </xsl:template>

    <xd:doc>
        <xd:desc>add margin not as a run with attributes when handling
        document nodes</xd:desc>
    </xd:doc>
    <xsl:template match="note" mode="para">
        <xsl:if test="@type='margin'">
            <xsl:element name="map">
                <xsl:call-template name="startmap">
                    <xsl:with-param name="objtype">run</xsl:with-param>
                    <xsl:with-param name="tag">note</xsl:with-param> 
                </xsl:call-template>
                <xsl:if test="not(@id)">
                    <xsl:call-template name="stringelement">
                        <xsl:with-param name="keynm">id</xsl:with-param>
                        <xsl:with-param name="value">_margin_<xsl:value-of select="accumulator-before('marginnote')"/></xsl:with-param>
                    </xsl:call-template>
                </xsl:if>
                <xsl:apply-templates select="@*"/>
            </xsl:element>
        </xsl:if>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>handle note nodes to create map based upon id</xd:desc>
    </xd:doc>
    <xsl:template match="note" mode="notes">
        <xsl:element name="map">
            <xsl:choose>
                <xsl:when test="not(@id) and @type='margin'">
                    <xsl:attribute name="key">_margin_<xsl:value-of select="accumulator-before('marginnote')"/></xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="key"><xsl:value-of select="./@id"/></xsl:attribute>                  
                </xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates select="@*[not(local-name()='id') and not(local-name()='object')]"/>
            <xsl:element name="array">
                <xsl:attribute name="key">children</xsl:attribute>
                <xsl:apply-templates select="*"/>
            </xsl:element>
        </xsl:element>
    </xsl:template>

    <xd:doc>
        <xd:desc>handle rendering elements</xd:desc>
    </xd:doc>
    <xsl:template match="hi | anchor | xref | ptr | ref | span | CharCode" mode="para">
        <xsl:variable name="current" select="."/>
        <xsl:apply-templates select="node()" mode="para">
            <xsl:with-param name="tag"  select="local-name()" />
            <xsl:with-param name="attr"  select="@*" />
        </xsl:apply-templates>
    </xsl:template>
    
    <xd:doc>
        <xd:desc>skip over pb</xd:desc>
    </xd:doc>
    <xsl:template match="pb"/>

</xsl:stylesheet>
