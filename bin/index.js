'use strict'
const sx = require("saxon-js");
const slugMap = require('./titles.json')
const https = require('https')
const fs = require('fs').promises
const createWriteStream = require('fs').createWriteStream
const path = require('path')
const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers');
const { resolve } = require("path/posix");
const argv = yargs(hideBin(process.argv)).argv

const fileBase = 'https://files.libertyfund.org/xml/'
const awsBase = 'https://oll-resources.s3.us-east-2.amazonaws.com/oll3/store/'

var slug = argv.s || ''
var format = argv.f || 'json'
var outputFolder = argv.o || ''
if (outputFolder.length > 0 && !outputFolder.endsWith('/')) {
    outputFolder = outputFolder + '/'
}

if (format != 'json' && format != 'html' && format != 'xml') {
    console.log("invalid format %s", format)
}

var xmlFileUID = slugMap[slug]
if (!slugMap[slug]) {
    console.log("invalid key %s", slug)
    process.exit(1)
}

var parts = path.parse(xmlFileUID)

format = format.toString().toLowerCase()
switch (format) {
    case 'json':
        xform(parts, outputFolder)
        break;
    case 'html':
        awsBase + xmlFileUID
        download(awsBase + xmlFileUID, outputFolder + parts.name + '.html')
        break;
    case 'xml':
        download(fileBase + parts.dir + "/" + parts.name + '.nodtd.xml', outputFolder + parts.name + '.nodtd.xml')
        break;
    default:
        console.log("valid formats are json, xml, html - %s is invalid", format)
        break;
}

function download(resourceLoc, outputFile, ext) {
    console.log("copying " + resourceLoc + " to " + outputFile)
    var wstr = createWriteStream(outputFile)
    new Promise((resolve, reject) => {
        var req = https.get(resourceLoc, res => {
            res.pipe(wstr);
            wstr.on('finish', function() {
                resolve('download successful')
                return
            });
            wstr.on('error', (e) => {
                reject(e)
                return
            })
        })
    }).then(resolve => {
        console.log(resolve)
    }).catch(e => {
        console.log(e)
    }).finally(() => {
        wstr.close()
    })

}

function xform(parsedPath, outputPath) {
    const xml_to_xdm_xsl = "https://files.libertyfund.org/xml/xmltoxdm.sef.json"
    const xdm_to_json_xsl = "https://files.libertyfund.org/xml/xdmtojson.sef.json"

    var pipelineStatus = "xdm"
    var resourceLoc = fileBase + parsedPath.dir + '/' + parsedPath.name + '.nodtd.xml'

    sx.getResource({
            type: "xml",
            location: resourceLoc
        })
        .then(document => {
            console.log("starting xsl transform to xdm format")
            return sx.transform({
                stylesheetLocation: xml_to_xdm_xsl,
                stylesheetParams: { "paraprefix": parsedPath.name },
                sourceType: "xml",
                sourceNode: document,
                destination: "serialized"
            }, "async")
        })
        .then(output => {

            return fs.writeFile(outputPath + parsedPath.name + ".xdm.xml", output.principalResult)
        })
        .then(() => {
            console.log("starting xsl transform to json format")
            pipelineStatus = "json"
            return sx.transform({
                stylesheetLocation: xdm_to_json_xsl,
                sourceFileName: outputPath + parsedPath.name + ".xdm.xml",
                destination: "serialized"
            }, "async")
        })
        .then(output => {
            return fs.writeFile(outputPath + parsedPath.name + ".json", JSON.stringify(JSON.parse(output.principalResult), null, 4))
        })
        .catch(reason => {
            console.log(reason)
            console.log(pipelineStatus)
            fs.writeFile(outputPath + parsedPath.name + "." + pipelineStatus + ".error", reason.toString())
        })
        .finally(() => {
            console.log('%s completed', parsedPath.name)
        });
}
